<?
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (! \Bitrix\Main\Loader::includeModule('pwd.typography')) {
	return false;
}