<?

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

Loc::loadMessages(__FILE__);

$arComponentParameters = array(
	'GROUPS' => array(),
	'PARAMETERS' => array(
        'BOOK_TITLE' => array(
            'PARENT' => 'BASE',
            'NAME' =>  Loc::getMessage('PWD_INCOME_ONE_AUTHOR_BOOK_TITLE'),
            'TYPE' => 'STRING',
            'DEFAULT' => 'Угрюм-река',
        ),
    )
);