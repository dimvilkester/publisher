<?

use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Pwd\Typography\PublisherTable;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

Loc::loadMessages(__FILE__);

class AuthorsIncomeOneAuthorComponent extends CBitrixComponent
{

    public function onPrepareComponentParams($arParams)
    {

        $this->arParams['FILTER'] = array();

        if (! empty($arParams['BOOK_TITLE'])) {
            $this->arParams['FILTER'] = array(
                '=BOOK_TITLE' => trim($arParams['BOOK_TITLE']),
            );
        }

        return $this->arParams;
    }
    
	public function executeComponent()
	{

        $this->includeComponentLang('class.php');

        $this->hasIncludeModules();

        $this->arResult['ITEMS'] = $this->getIncomeOneAuthor();

		$this->includeComponentTemplate();

	}


    /**
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    private function getIncomeOneAuthor()
    {
        return PublisherTable::getList(array(
            'select' => array(
                'BOOK_TITLE' => 'BOOKS.TITLE',
                'BOOK_COPIES_CNT' => 'BOOKS.COPIES_CNT',
                'AUTHOR_PROFIT',
                'AUTHORS_COUNT',
                'INCOME_ONE_AUTHOR',
            ),
            'group' => array(
                'ID',
                'BOOKS.ID'
            ),
            'filter' => $this->arParams['FILTER'],
            'runtime' => array(
                new ExpressionField(
                    'AUTHORS_COUNT',
                    'COUNT(%s)',
                    array(
                        'BOOKS.AUTHORS.AUTHOR.ID'
                    )
                ),
                new ExpressionField(
                    'INCOME_ONE_AUTHOR',
                    'ROUND((ROUND(%1$s / COUNT(%2$s), 2) * %3$s), 2)',
                    array(
                        'AUTHOR_PROFIT',
                        'BOOKS.AUTHORS.AUTHOR.ID',
                        'BOOKS.COPIES_CNT'
                    )
                )
            )
        ))->fetchAll();
    }

    /**
     * @throws \Bitrix\Main\LoaderException
     */
    protected function hasIncludeModules()
    {
        if (! Loader::includeModule('pwd.typography')) {
            throw new \Bitrix\Main\LoaderException(Loc::getMessage('PWD_INCOME_ONE_AUTHOR_MODULE_NOT_INSTALLED'));
        }
    }

}