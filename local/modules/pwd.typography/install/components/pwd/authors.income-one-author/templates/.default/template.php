<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="table-wrap">
    <table>
        <thead>
            <tr>
                <th>Название книги</th>
                <th>Тираж книги</th>
                <th>Гонорар за экземпляр</th>
                <th>Количество авторов написавших книгу</th>
                <th>Доход одного автора за полностью проданный тираж</th>
            </tr>
        </thead>

        <tbody>
            <?foreach ($arResult['ITEMS'] as $arItem):?>
                <tr>
                    <td data-label="Название книги"><?= $arItem['BOOK_TITLE']?></td>
                    <td data-label="Тираж книги"><?= $arItem['BOOK_COPIES_CNT']?></td>
                    <td data-label="Гонорар за экземпляр"><?= $arItem['AUTHOR_PROFIT']?></td>
                    <td data-label="Количество авторов написавших книгу"><?= $arItem['AUTHORS_COUNT']?></td>
                    <td data-label="Доход одного автора за полностью проданный тираж"><?= $arItem['INCOME_ONE_AUTHOR']?></td>
                </tr>
            <?endforeach?>
        </tbody>
    </table>
</div>