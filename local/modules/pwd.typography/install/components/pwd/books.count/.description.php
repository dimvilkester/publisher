<?

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
	'NAME' => Loc::getMessage('PWD_BOOKS_COUNT_NAME'),
	'DESCRIPTION' => Loc::getMessage('PWD_BOOKS_COUNT_DESC'),
	'ICON' => '',
	'SORT' => 10,
	'CACHE_PATH' => 'Y',
	'PATH' => array(
		'ID' => 'content'
	),
	'COMPLEX' => 'N'
);