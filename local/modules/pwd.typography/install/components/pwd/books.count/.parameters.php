<?

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

Loc::loadMessages(__FILE__);

$arComponentParameters = array(
	'GROUPS' => array(),
	'PARAMETERS' => array(
        'AUTHOR_LAST_NAME' => array(
            'PARENT' => 'BASE',
            'NAME' =>  Loc::getMessage('PWD_BOOKS_COUNT_AUTHOR_LAST_NAME'),
            'TYPE' => 'STRING',
            'DEFAULT' => 'Уайльд',
        ),
        'PUBLISHER_TITLE' => array(
            'PARENT' => 'BASE',
            'NAME' =>  Loc::getMessage('PWD_BOOKS_COUNT_PUBLISHER_TITLE'),
            'TYPE' => 'STRING',
            'DEFAULT' => 'АСТ',
        )
    )
);