<?

use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Pwd\Typography\BookTable;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

Loc::loadMessages(__FILE__);

class BooksCountComponent extends CBitrixComponent
{

    public function onPrepareComponentParams($arParams)
    {
        $this->arParams['AUTHOR_LAST_NAME'] = trim($arParams['AUTHOR_LAST_NAME']);
        $this->arParams['PUBLISHER_TITLE'] = trim($arParams['PUBLISHER_TITLE']);

        return $this->arParams;
    }
    
	public function executeComponent()
	{

        $this->includeComponentLang('class.php');

        $this->hasIncludeModules();

        $this->arResult['BOOKS_COUNT'] = $this->getBooksCount();

		$this->includeComponentTemplate();

	}

    /**
     * @return int|mixed
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    private function getBooksCount()
    {
        $booksCount = 0;
        
        $rsBooksCount = BookTable::getList(array(
            'filter' => array(
                '=AUTHORS.AUTHOR.LAST_NAME' => $this->arParams['AUTHOR_LAST_NAME'],
                '=PUBLISHER.TITLE' => $this->arParams['PUBLISHER_TITLE'],
            ),
            'select' => array(
                'AUTHOR_LAST_NAME' => 'AUTHORS.AUTHOR.LAST_NAME',
                'BOOKS_COUNT'
            ),
            'group' => array(
                'AUTHORS.AUTHOR.LAST_NAME'
            ),
            'runtime' => array(
                new ExpressionField(
                    'BOOKS_COUNT',
                    'SUM(%s)',
                    array(
                        'COPIES_CNT'
                    )
                ),
            ),
        ));
        if ($arBooksCount = $rsBooksCount->fetch()) {
            $booksCount = $arBooksCount['BOOKS_COUNT'];
        }

        return $booksCount;
    }

    /**
     * @throws \Bitrix\Main\LoaderException
     */
    protected function hasIncludeModules()
    {
        if (! Loader::includeModule('pwd.typography')) {
            throw new \Bitrix\Main\LoaderException(Loc::getMessage('PWD_BOOKS_COUNT_MODULE_NOT_INSTALLED'));
        }
    }

}