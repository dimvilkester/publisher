<?
$MESS['PWD_BOOKS_COUNT_NAME'] = 'Books count';
$MESS['PWD_BOOKS_COUNT_DESC'] = 'Get the number of books by one author with the last name AUTHOR_LAST_NAME, published by PUBLISHER_TITLE';