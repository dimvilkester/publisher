<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<p>
    У автора по фамилии <b><?= $arParams['AUTHOR_LAST_NAME']?></b>
    в издательстве <b><?= $arParams['PUBLISHER_TITLE']?></b>
    напечатано <b><?= $arResult['BOOKS_COUNT']?></b> книг.
</p>