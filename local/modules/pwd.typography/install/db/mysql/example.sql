insert into pwd_publishers (ID, TITLE, CITY, AUTHOR_PROFIT) values (1, 'Аркадия', 'Москва', 100);
insert into pwd_publishers (ID, TITLE, CITY, AUTHOR_PROFIT) values (2, 'Ильфиада', 'Санкт-Петербург', 150);
insert into pwd_publishers (ID, TITLE, CITY, AUTHOR_PROFIT) values (3, 'АСТ', 'Москва', 150);

insert into pwd_books (ID, PUBLISHER_ID, TITLE, YEAR, COPIES_CNT) values (1, 1, 'Страна Чудес без тормозов и Конец Света', '2020-01-01', 50);
insert into pwd_books (ID, PUBLISHER_ID, TITLE, YEAR, COPIES_CNT) values (2, 1, '1Q84. Тысяча Невестьсот Восемьдесят Четыре.', '2020-01-01', 50);
insert into pwd_books (ID, PUBLISHER_ID, TITLE, YEAR, COPIES_CNT) values (3, 2, 'Непобедимое солнце', '2020-01-01',  150);
insert into pwd_books (ID, PUBLISHER_ID, TITLE, YEAR, COPIES_CNT) values (4, 2, 'Угрюм-река', '2020-01-01',  100);
insert into pwd_books (ID, PUBLISHER_ID, TITLE, YEAR, COPIES_CNT) values (5, 2, 'Сказки', '2020-01-01',  200);
insert into pwd_books (ID, PUBLISHER_ID, TITLE, YEAR, COPIES_CNT) values (6, 3, 'Портрет Дориана Грея', '2020-01-01',  100);
insert into pwd_books (ID, PUBLISHER_ID, TITLE, YEAR, COPIES_CNT) values (7, 3, 'Мальчик-Звезда', '2020-01-01',  150);
insert into pwd_books (ID, PUBLISHER_ID, TITLE, YEAR, COPIES_CNT) values (8, 3, 'Замечательная ракета', '2020-01-01',  150);
insert into pwd_books (ID, PUBLISHER_ID, TITLE, YEAR, COPIES_CNT) values (9, 3, 'Кентервильское привидение', '2020-01-01',  150);
insert into pwd_books (ID, PUBLISHER_ID, TITLE, YEAR, COPIES_CNT) values (10, 3, 'Леденцовые туфельки', '2020-01-01',  50);

insert into pwd_authors (ID, FIRST_NAME, SECOND_NAME, LAST_NAME, CITY) values (1, 'Харуки', '', 'Мураками', 'Токио');
insert into pwd_authors (ID, FIRST_NAME, SECOND_NAME, LAST_NAME, CITY) values (2, 'Виктор', 'Олегович', 'Пелевин', 'Москва');
insert into pwd_authors (ID, FIRST_NAME, SECOND_NAME, LAST_NAME, CITY) values (3, 'Вячеслав', 'Яковлевич', 'Шишков', 'Москва');
insert into pwd_authors (ID, FIRST_NAME, SECOND_NAME, LAST_NAME, CITY) values (4, 'Оскар', '', 'Уайльд', 'Лондон');
insert into pwd_authors (ID, FIRST_NAME, SECOND_NAME, LAST_NAME, CITY) values (5, 'Джоанн', '', 'Харрис', 'Нью-Йорк');
insert into pwd_authors (ID, FIRST_NAME, SECOND_NAME, LAST_NAME, CITY) values (6, 'Яков', 'Анатольевич', 'Шишков', 'Москва');

insert into pwd_book_author (BOOK_ID, AUTHOR_ID) values (1, 1);
insert into pwd_book_author (BOOK_ID, AUTHOR_ID) values (2, 1);
insert into pwd_book_author (BOOK_ID, AUTHOR_ID) values (3, 2);
insert into pwd_book_author (BOOK_ID, AUTHOR_ID) values (4, 3);
insert into pwd_book_author (BOOK_ID, AUTHOR_ID) values (4, 6);
insert into pwd_book_author (BOOK_ID, AUTHOR_ID) values (5, 4);
insert into pwd_book_author (BOOK_ID, AUTHOR_ID) values (6, 4);
insert into pwd_book_author (BOOK_ID, AUTHOR_ID) values (7, 4);
insert into pwd_book_author (BOOK_ID, AUTHOR_ID) values (8, 4);
insert into pwd_book_author (BOOK_ID, AUTHOR_ID) values (9, 4);
insert into pwd_book_author (BOOK_ID, AUTHOR_ID) values (10, 5);
