create table if not exists pwd_publisher
(
	ID int(11) unsigned not null auto_increment,
	TITLE varchar(255) not null,
	CITY varchar(100) null,
	AUTHOR_PROFIT int not null,
	primary key (ID)
}

create table if not exists pwd_books
(
	ID int(11) unsigned not null auto_increment,
	TITLE varchar(255) not null,
	YEAR not date not null,
	COPIES_CNT int not null,
	PUBLISHER_ID int(11) unsigned not null,
	primary key (ID)
	foreign key (PUBLISHER_ID) references pwd_publisher(ID)
}

create table if not exists pwd_authors
(
	ID int(11) unsigned not null auto_increment,
	FIRST_NAME varchar(50) not null,
	LAST_NAME varchar(50) not null,
	SECOND_NAME varchar(50) null,
	CITY varchar(100) null,
	primary key (ID)
}

create table if not exists pwd_book_author
(
	BOOK_ID int(11) unsigned not null,
    AUTHOR_ID int(11) unsigned not null,
    primary key (BOOK_ID, AUTHOR_ID)
    foreign key (BOOK_ID) references pwd_books(ID)
    foreign key (AUTHOR_ID) references pwd_authors(ID)
}