drop table if exists pwd_publisher;
drop table if exists pwd_books;
drop table if exists pwd_authors;
drop table if exists pwd_book_author;