<?

use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Bitrix\Main\Entity\Base;
use Bitrix\Main\Application;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\IO\Directory;
use Bitrix\Main\IO\InvalidPathException;
use Bitrix\Main\IO\File;
use Pwd\Typography\AuthorTable;
use Pwd\Typography\BookAuthorTable;
use Pwd\Typography\BookTable;
use Pwd\Typography\PublisherTable;


Loc::loadMessages(__FILE__);

class pwd_typography extends CModule
{
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_ID = 'pwd.typography';
    public $MODULE_SORT = 100;
    public $SHOW_SUPER_ADMIN_GROUP_RIGHTS;
    public $MODULE_GROUP_RIGHTS = 'N';
    public $PARTNER_NAME;
    public $PARTNER_URI;

    private $exclusionAdminFiles = [
        '..',
        '.',
        'menu.php'
    ];

    public function __construct()
    {
        $arModuleVersion = array();
        include(__DIR__ . '/version.php');

        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_NAME = Loc::getMessage('PWD_BOOKS_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PWD_BOOKS_MODULE_DESCRIPTION');

        $this->PARTNER_NAME = Loc::getMessage('PWD_BOOKS_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('PWD_BOOKS_PARTNER_URI');
    }

    public function DoInstall()
    {

        global $APPLICATION;

        $this->isVersionD7();

        ModuleManager::registerModule($this->MODULE_ID);

        $this->InstallDB();
        $this->InstallEvents();
        $this->InstallFiles();

        $APPLICATION->IncludeAdminFile(Loc::getMessage('PWD_BOOKS_INSTALL_TITLE'), $this->getPath() . '/install/step.php');

    }

    public function DoUninstall()
    {

        global $APPLICATION;

        $context = Application::getInstance()->getContext();
        $request = $context->getRequest();

        if ($request['step'] < 2) {

            $APPLICATION->IncludeAdminFile(Loc::getMessage('PWD_BOOKS_UNINSTALL_TITLE'), $this->getPath() . '/install/unstep1.php');

        } elseif ($request['step'] == 2) {

            $this->UnInstallEvents();
            $this->UnInstallFiles();

            if ($request['savedata'] != 'Y') {
                $this->UnInstallDB();
            }

            ModuleManager::unRegisterModule($this->MODULE_ID);

            $APPLICATION->IncludeAdminFile(Loc::getMessage('PWD_BOOKS_UNINSTALL_TITLE'), $this->getPath() . '/install/unstep2.php');
        }

    }

    public function InstallDB()
    {

        Loader::includeModule($this->MODULE_ID);

        // Помимо создания таблиц в базе данных из сущности, можно создавать их при помощи файла install.sql
        if (! Application::getConnection(PublisherTable::getConnectionName())
            ->isTableExists(Base::getInstance('\Pwd\Typography\PublisherTable')->getDBTableName())
        ) {
            Base::getInstance('\Pwd\Typography\PublisherTable')->createDbTable();
        }

        if (! Application::getConnection(BookTable::getConnectionName())
            ->isTableExists(Base::getInstance('\Pwd\Typography\BookTable')->getDBTableName())
        ) {
            Base::getInstance('\Pwd\Typography\BookTable')->createDbTable();
        }

        if (! Application::getConnection(AuthorTable::getConnectionName())
            ->isTableExists(Base::getInstance('\Pwd\Typography\AuthorTable')->getDBTableName())
        ) {
            Base::getInstance('\Pwd\Typography\AuthorTable')->createDbTable();
        }

        if (! Application::getConnection(BookAuthorTable::getConnectionName())
            ->isTableExists(Base::getInstance('\Pwd\Typography\BookAuthorTable')->getDBTableName())
        ) {
            Base::getInstance('\Pwd\Typography\BookAuthorTable')->createDbTable();
        }

        $this->exampleDataInstallToDb('example');

    }

    public function UnInstallDB()
    {

        Loader::includeModule($this->MODULE_ID);

        Application::getConnection(PublisherTable::getConnectionName())
            ->queryExecute('drop table if exists ' . Base::getInstance('\Pwd\Typography\PublisherTable')->getDBTableName());

        Application::getConnection(BookTable::getConnectionName())
            ->queryExecute('drop table if exists ' . Base::getInstance('\Pwd\Typography\BookTable')->getDBTableName());

        Application::getConnection(AuthorTable::getConnectionName())
            ->queryExecute('drop table if exists ' . Base::getInstance('\Pwd\Typography\AuthorTable')->getDBTableName());

        Application::getConnection(BookAuthorTable::getConnectionName())
            ->queryExecute('drop table if exists ' . Base::getInstance('\Pwd\Typography\BookAuthorTable')->getDBTableName());

        Option::delete($this->MODULE_ID);
    }

    public function InstallEvents()
    {
        return true;
    }

    public function UnInstallEvents()
    {
        return true;
    }

    public function InstallFiles()
    {
        $pathInstallComponents = $this->getPath() . '/install/components';

        try {
            $this->isDirectoryExists($pathInstallComponents);
        } catch (InvalidPathException $e) {
            ShowError($e->getMessage());
        }

        CopyDirFiles($pathInstallComponents, $_SERVER['DOCUMENT_ROOT'] . '/bitrix/components', true, true);

        try {
            $this->copyAdminFiles();
        } catch (InvalidPathException $e) {
            ShowError($e->getMessage());
        }

    }

    public function UnInstallFiles()
    {
        // Delete folders components
        Directory::deleteDirectory($_SERVER['DOCUMENT_ROOT'] . '/bitrix/components/pwd/');

        try {
            $this->deleteAdminFiles();
        } catch (InvalidPathException $e) {
            ShowError($e->getMessage());
        }

    }

    /**
     * @param string $fileName
     */
    private function exampleDataInstallToDb(string $fileName = 'example') {
        $this->filesSqlInstallToDb($fileName);
    }

    /**
     * @param string $fileName
     */
    private function filesSqlInstallToDb(string $fileName = 'example')
    {
        global $DB;

        $pathInstallDb = $this->getPath() . '/install/db/';

        try {

            $this->isDirectoryExists($pathInstallDb);

            if (file_exists($pathInstallDb . strtolower($DB->type) . '/' . $fileName . '.sql')) {
                $DB->RunSQLBatch($pathInstallDb . strtolower($DB->type) . '/' . $fileName . '.sql');
            }

        } catch (InvalidPathException $e) {
            ShowError($e->getMessage());
        }

    }

    /**
     * @param string $path
     * @return bool
     * @throws InvalidPathException
     */
    private function isDirectoryExists(string $path)
    {
        if (!Directory::isDirectoryExists($path)) {
            throw new InvalidPathException($path);
        }

        return true;
    }

    /**
     * @return bool
     * @throws InvalidPathException
     */
    private function copyAdminFiles()
    {

        $pathAdminFolder = $this->getPath() . '/admin';

        $this->isDirectoryExists($pathAdminFolder);

        // Delete files and folders for admin panel
        $this->copyFilesFromInstallAdminFolder();
        $this->copyFilesFromAdminFolder($pathAdminFolder);

        return true;
    }

    private function copyFilesFromInstallAdminFolder()
    {
        CopyDirFiles($this->getPath() . '/install/admin/', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin');
    }

    /**
     * @param string $pathFolder
     * @return bool
     */
    private function copyFilesFromAdminFolder(string $pathFolder)
    {

        if ($dir = opendir($pathFolder)) {

            while (false !== $file = readdir($dir)) {

                if (in_array($file, $this->exclusionAdminFiles)) {
                    continue;
                }

                file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $this->MODULE_ID . '_' . $file,
                    '<' . '? require($_SERVER["DOCUMENT_ROOT"]."' . $this->getPath(true) . '/admin/' . $file . '");?' . '>');
            }

            closedir($dir);

            return true;
        }

        return false;
    }

    /**
     * @return bool
     * @throws InvalidPathException
     */
    private function deleteAdminFiles()
    {

        $pathAdminFolder = $this->getPath() . '/admin';

        // Delete files and folders for admin panel
        $this->isDirectoryExists($pathAdminFolder);

        DeleteDirFiles($this->getPath() . '/install/admin/', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/');

        $this->deleteFilesFromAdminFolder($pathAdminFolder);

        return true;

    }

    /**
     * @param string $pathFolder
     * @return bool
     */
    private function deleteFilesFromAdminFolder(string $pathFolder)
    {

        if ($dir = opendir($pathFolder)) {

            while (($file = readdir($dir)) !== false) {

                if (in_array($file, $this->exclusionAdminFiles)) {
                    continue;
                }

                $arDebug[] = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $this->MODULE_ID . '_' . $file;

                File::deleteFile($_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $this->MODULE_ID . '_' . $file);
            }

            closedir($dir);

            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    private function isVersionD7()
    {
        global $APPLICATION;

        if (!CheckVersion(ModuleManager::getVersion('main'), '14.00.00')) {
            $APPLICATION->ThrowException(Loc::getMessage('PWD_BOOKS_INSTALL_ERROR_VERSION'));
        }

        return true;
    }

    /**
     * @param bool $isDocumentRoot
     * @return string|string[]
     */
    private function getPath($isDocumentRoot = false)
    {

        $dir = str_ireplace('\\', '/', dirname(__DIR__));

        if ($isDocumentRoot) {

            $documentRoot = str_ireplace('\\', '/', Application::getDocumentRoot());

            return str_ireplace($documentRoot, '', $dir);

        } else {

            return $dir;

        }
    }

    /**
     * @return string[][]
     */
    public function GetModuleRightList() {
        return array(
            'reference_id' => array('D', 'R', 'W'),
            'reference' => array(
                '[D] ' . Loc::getMessage('PWD_BOOKS_DENIED'),
                '[R] ' . Loc::getMessage('PWD_BOOKS_WRITE_SETTINGS'),
                '[W] ' . Loc::getMessage('PWD_BOOKS_FULL'))
        );
    }

}
