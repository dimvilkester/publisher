<?
$MESS['PWD_BOOKS_MODULE_NAME'] = 'Typography';
$MESS['PWD_BOOKS_MODULE_DESCRIPTION'] = 'Module for working with books, authors, publishers';
$MESS['PWD_BOOKS_PARTNER_NAME'] = 'Dima Vilkin';
$MESS['PWD_BOOKS_PARTNER_URI'] = 'https://bitbucket.org/dimvilkester/';
$MESS['PWD_BOOKS_INSTALL_TITLE'] = '"Typography" module installation';
$MESS['PWD_BOOKS_UNINSTALL_TITLE'] = '"Typography" module uninstallation';
$MESS['PWD_BOOKS_INSTALL_ERROR_VERSION'] = 'Core error version. Core D7 not found';

$MESS['PWD_BOOKS_DENIED'] = 'Access is denied';
$MESS['PWD_BOOKS_WRITE_SETTINGS'] = 'Change module settings';
$MESS['PWD_BOOKS_FULL'] = 'Full access';