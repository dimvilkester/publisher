<?
$MESS['PWD_BOOKS_MODULE_NAME'] = 'Типография';
$MESS['PWD_BOOKS_MODULE_DESCRIPTION'] = 'Модуль для работы с книгами, авторами, издательствами';
$MESS['PWD_BOOKS_PARTNER_NAME'] = 'Дима Вилкин';
$MESS['PWD_BOOKS_PARTNER_URI'] = 'https://bitbucket.org/dimvilkester/';
$MESS['PWD_BOOKS_INSTALL_TITLE'] = 'Установка модуля "Типография"';
$MESS['PWD_BOOKS_UNINSTALL_TITLE'] = 'Деинсталляция модуля "Типография"';
$MESS['PWD_BOOKS_INSTALL_ERROR_VERSION'] = 'Ошибка версии ядра. Ядро D7 отсутствует';

$MESS['PWD_BOOKS_DENIED'] = 'Доступ закрыт';
$MESS['PWD_BOOKS_WRITE_SETTINGS'] = 'Просмотр всех данных доступа';
$MESS['PWD_BOOKS_FULL'] = 'Полный доступ';