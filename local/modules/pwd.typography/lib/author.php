<?


namespace Pwd\Typography;

use Bitrix\Main\Entity;
use Bitrix\Main\ORM\Fields\Relations;

class AuthorTable extends Entity\DataManager
{

    public static function getTableName()
    {
        return 'pwd_authors';
    }

    public static function getConnectionName()
    {
        return 'default';
    }

    public static function getMap()
    {
        return array(
            // ID
            (new Entity\IntegerField('ID'))
                ->configurePrimary()
                ->configureAutocomplete(),
            // FIRST_NAME
            (new Entity\StringField('FIRST_NAME'))
                ->configureRequired()
                ->addValidator(new Entity\Validator\Length(2, 50)),
            // SECOND_NAME
            (new Entity\StringField('SECOND_NAME'))
                ->configureRequired()
                ->addValidator(new Entity\Validator\Length(2, 50)),
            // LAST_NAME
            (new Entity\StringField('LAST_NAME'))
                ->addValidator(new Entity\Validator\Length(2, 50))
                ->configureDefaultValue('unknown'),
            // CITY
            (new Entity\StringField('CITY'))
                ->addValidator(new Entity\Validator\Length(2, 50)),
            // BOOKS
            (new Relations\OneToMany(
                'BOOKS',
                BookAuthorTable::class,
                'AUTHOR'
            ))
        );
    }

}
