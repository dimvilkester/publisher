<?


namespace Pwd\Typography;

use Bitrix\Main\Entity;
use Bitrix\Main\ORM\Fields\Relations;
use Bitrix\Main\Type;

class BookTable extends Entity\DataManager
{

    public static function getTableName()
    {
        return 'pwd_books';
    }

    public static function getConnectionName()
    {
        return 'default';
    }

    public static function getMap()
    {
        return array(
            // ID
            (new Entity\IntegerField('ID'))
                ->configurePrimary()
                ->configureAutocomplete(),
            // TITLE
            (new Entity\StringField('TITLE'))
                ->configureRequired()
                ->addValidator(new Entity\Validator\Length(2, 50)),
            // YEAR
            new Entity\DateField('YEAR', array(
                'default_value' => new Type\Date
            )),
            // COPIES_CNT
            (new Entity\IntegerField('COPIES_CNT')),
            // PUBLISHER_ID
            (new Entity\IntegerField('PUBLISHER_ID')),
            // PUBLISHER
            (new Relations\Reference(
                'PUBLISHER',
                PublisherTable::class,
                Entity\Query\Join::on('this.PUBLISHER_ID', 'ref.ID')
            ))
                ->configureJoinType(Entity\Query\Join::TYPE_LEFT),
            // AUTHORS
            (new Relations\OneToMany(
                'AUTHORS',
                BookAuthorTable::class,
                'BOOK'
            )),
        );
    }

}
