<?


namespace Pwd\Typography;

use Bitrix\Main\Entity;
use Bitrix\Main\ORM\Fields\Relations;

class BookAuthorTable extends Entity\DataManager
{

    public static function getTableName()
    {
        return 'pwd_book_author';
    }

    public static function getConnectionName()
    {
        return 'default';
    }

    public static function getMap()
    {
        return array(
            // BOOK_ID
            (new Entity\IntegerField('BOOK_ID'))
                ->configurePrimary(true),
            // BOOK
            (new Relations\Reference(
                'BOOK',
                BookTable::class,
                Entity\Query\Join::on('this.BOOK_ID', 'ref.ID')
            ))
                ->configureJoinType(Entity\Query\Join::TYPE_INNER),
            // AUTHOR_ID
            (new Entity\IntegerField('AUTHOR_ID'))
                ->configurePrimary(true),
            // AUTHOR
            (new Relations\Reference(
                'AUTHOR',
                AuthorTable::class,
                Entity\Query\Join::on('this.AUTHOR_ID', 'ref.ID')
            ))
                ->configureJoinType(Entity\Query\Join::TYPE_INNER),
        );
    }

}