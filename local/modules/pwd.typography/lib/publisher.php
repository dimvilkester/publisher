<?


namespace Pwd\Typography;

use Bitrix\Main\Entity;
use Bitrix\Main\ORM\Fields\Relations;

class PublisherTable extends Entity\DataManager
{

    public static function getTableName()
    {
        return 'pwd_publishers';
    }

    public static function getConnectionName()
    {
        return 'default';
    }

    public static function getMap()
    {
        return array(
            // ID
            (new Entity\IntegerField('ID'))
                ->configurePrimary()
                ->configureAutocomplete(),
            // TITLE
            (new Entity\StringField('TITLE'))
                ->configureRequired()
                ->addValidator(new Entity\Validator\Length(2, 50)),
            // CITY
            (new Entity\StringField('CITY'))
                ->addValidator(new Entity\Validator\Length(2, 100)),
            // AUTHOR_PROFIT
            (new Entity\IntegerField('AUTHOR_PROFIT')),
            // BOOKS
            (new Relations\OneToMany(
                'BOOKS',
                BookTable::class,
                'PUBLISHER'
            ))
                ->configureJoinType(Entity\Query\Join::TYPE_LEFT)
        );
    }

}
